import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();

        Buyer buyer = session.get(Buyer.class, 2L);
        System.out.println(buyer.getProducts());
        Product product = session.get(Product.class, 2L);
        System.out.println(product.getBuyers());

        session.getTransaction().commit();
        session.close();
    }
}
